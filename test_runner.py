# pylint: disable-msg=C0103
import unittest
import test_module

TestSuite = unittest.TestSuite()
TestSuite.addTest(unittest.makeSuite(test_module.TestMethods))

runner = unittest.TextTestRunner()
runner.run(TestSuite)

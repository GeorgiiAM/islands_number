# pylint: disable-msg=C0103
# pylint: disable-msg=R1703
# pylint: disable-msg=R1716


class binary_matrix():
    def __init__(self, array):
        self.matrix = array
        self.row = len(self.matrix)
        self.column = len(self.matrix[0])
        self.visit = [[False for j in range(self.column)]
                      for i in range(self.row)]

    def in_range(self, point):
        x, y = point[0], point[1]
        bool_x = x >= 0 and x < self.row
        bool_y = y >= 0 and y < self.column
        if bool_x and bool_y:
            rez = True
        else:
            rez = False
        return rez

    def neighbours(self, point):
        x, y = point[0], point[1]
        point_list = []
        possible_point = (x, y + 1), (x + 1, y), (x, y - 1), (x - 1, y)
        for p in possible_point:
            if self.in_range(p):
                point_list.append(p)
        return point_list

    def find_algorithm(self, point, element):
        x, y = point[0], point[1]
        self.visit[x][y] = True

        if self.matrix[x][y] == element:
            for pt in self.neighbours(point):
                if not self.visit[pt[0]][pt[1]]:
                    self.find_algorithm(pt, element)
            self.matrix[x][y] = 8

    def find_element_in_2D(self, element):
        ind_col = 0
        ind_row = 0
        for line in self.matrix:
            try:
                ind_col = line.index(element)
                return(ind_row, ind_col)
            except ValueError:
                ind_row += 1
        return(None, None)

    def find_island(self, element=1):
        index = self.find_element_in_2D(element)
        island = 0
        while index[0] is not None and index[1] is not None:
            self.find_algorithm(index, element)
            island += 1
            index = self.find_element_in_2D(element)
        return island


def print_matr(matrix):
    for s in matrix:
        print(s)


def run(matrix, element=1):
    try:
        gr = binary_matrix(matrix)
        res = gr.find_island(element)
    except (TypeError, IndexError):
        res = 0
    return res
